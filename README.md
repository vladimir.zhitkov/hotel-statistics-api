# Task description
## Coding challenge - Statistics APIs
Please use PHP 7.4, Symfony 5, Doctrine and an RDMS of your choice to create one service which provides a REST API endpoint as explained bellow.
 
## Todo
- Create these two tables: 
    1) `hotel`(`id`, `name`)
    2) `review`(`id`, `hotel_id`, `score`, `comment`, `created_date`)
- Fill the `hotel` table with 10 rows with random names
- Fill the `review` table with a total number of 100.000 reviews which are distributed randomly over the last two years. Score and comments should be randomly filled and each hotel should have a random number of reviews as well.
- ### Overtime Endpoint:
  It gets a hotel-id and a date range from http requests and returns the overtime average score of the hotel for grouped date ranges. The date range is grouped as follows:
  - 1 - 29 days: Grouped daily
  - 30 - 89 days: Grouped weekly
  - More than 89 days: Grouped monthly
  
  The response should contain "review-count", "average-score" and "date-group" (either the day, calendar-week or the month) per data point.
- Use a DTO layer and a serializer to generate the response for the endpoint.
- Use Doctrine QueryBuilder for fetching the data.
- Test the application by functional or unit tests using PHPUnit.
- Note that it is a minimal amount of data to work with, the implementation should ideally work with larger amount of hotels and reviews.
- Upload the project on github, gitlab or bitbucket and send it to us. You can use a temporary or throwaway account to maintain privacy.

# Solution
## Installation
Install dependencies:

```
composer install
```

## How to run the application

I used Docker to make the service setup easier.

This setup is tested on Linux based machine. 

### Build images and start Docker containers:

```
USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose up -d
```

### Create mysql users and database:

Login to mysql server console:

```
docker exec -it hotel_statistics_api_db_1 bash
```

Execute the following commands:

```
mysql -uroot -proot
create database hotel_statistics;
CREATE USER 'hotel'@'localhost' IDENTIFIED BY 'HotelStatistics';
GRANT ALL PRIVILEGES ON `hotel_statistics` . * TO 'hotel'@'localhost';
CREATE USER 'hotel'@'%' IDENTIFIED BY 'HotelStatistics';
GRANT ALL PRIVILEGES ON `hotel_statistics` . * TO 'hotel'@'%';
FLUSH PRIVILEGES;
exit
``` 

### Run migrations:

```
bin/console doctrine:migrations:migrate
```

### Load fixtures:

```
bin/console doctrine:fixtures:load
```

Now the service is fully working. It is available on http://localhost:8010

Format of the API endpoint request is the following:

```
hotel/{hotel-id}/average-score?start-date={Y-m-d}&end-date={Y-m-d}
```
Example:

```
http://localhost:8010/hotel/1/average-score?start-date=2020-01-01&end-date=2020-05-09
```

## How to run the tests:

I wrote functional tests to test the endpoint. The endpoint performs well. It can work with larger amount of hotel and reviews.

Login into the Docker PHP container:

```
docker exec -it hotel_statistics_api_php_1 bash
```

Run the tests:

```
php bin/phpunit --color
```
