<?php

namespace App\Controller;

use App\Exception\RequestHandlerException;
use App\Repository\HotelRepository;
use App\Service\AverageScoreServiceInterface;
use App\Service\RequestHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class HotelController
{
    /**
     * @var HotelRepository
     */
    private $hotelRepository;

    /**
     * @var RequestHandlerInterface
     */
    private $requestHandler;

    /**
     * @var AverageScoreServiceInterface
     */
    private $averageScoreService;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param HotelRepository $hotelRepository
     * @param RequestHandlerInterface $requestHandler
     * @param AverageScoreServiceInterface $averageScoreService
     * @param SerializerInterface $serializer
     */
    public function __construct(HotelRepository $hotelRepository, RequestHandlerInterface $requestHandler, AverageScoreServiceInterface $averageScoreService, SerializerInterface $serializer)
    {
        $this->hotelRepository = $hotelRepository;
        $this->requestHandler = $requestHandler;
        $this->averageScoreService = $averageScoreService;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/hotel/{id}/average-score", name="hotel_average_score", methods={"GET"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function getAverageScore(Request $request, int $id): JsonResponse
    {
        try {
            $averageScoreRequest = $this->requestHandler->handleAverageScoreRequest($request, $id);
        } catch (RequestHandlerException $e) {
            return new JsonResponse(["message" => $e->getMessage()], $e->getCode());
        }

        $averageScores = $this->averageScoreService->get($averageScoreRequest);

        $averageScoresResponse = $this->serializer->serialize($averageScores, 'json');

        return new JsonResponse($averageScoresResponse, Response::HTTP_OK, [], true);
    }
}
