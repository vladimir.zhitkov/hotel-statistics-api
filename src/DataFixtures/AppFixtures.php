<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private const HOTELS_TOTAL_NUMBER = 10;
    private const REVIEWS_TOTAL_NUMBER = 100000;
    private const REVIEWS_BATCH_SIZE = 1000;
    private const REVIEW_START_DATE = '-2 years';
    private const REVIEW_MIN_SCORE = 1;
    private const REVIEW_MAX_SCORE = 5;
    private const REVIEW_MAX_COMMENT_LENGTH = 255;

    public function load(ObjectManager $manager)
    {
        $manager->getClassMetadata(Hotel::class)->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);

        $faker = Factory::create();

        for ($i = 1; $i <= self::HOTELS_TOTAL_NUMBER; $i++) {
            $hotel = (new Hotel())
                ->setId($i)
                ->setName($faker->company)
            ;
            $manager->persist($hotel);
        }

        $manager->flush();

        $hotels = $manager->getRepository("App:Hotel")
            ->findAll();

        for ($batchIndex = 1; $batchIndex <= self::REVIEWS_TOTAL_NUMBER; $batchIndex++) {
            /**
             * @var Hotel $hotel
             */
            $hotel = $faker->randomElement($hotels);
            $score = $faker->numberBetween(self::REVIEW_MIN_SCORE, self::REVIEW_MAX_SCORE);
            $comment = $faker->text(self::REVIEW_MAX_COMMENT_LENGTH);
            $date = $faker->dateTimeBetween($startDate = self::REVIEW_START_DATE, $endDate = 'now');

            $review = (new Review())
                ->setHotel($hotel)
                ->setScore($score)
                ->setComment($comment)
                ->setCreatedDate($date)
            ;
            $manager->persist($review);

            if (($batchIndex % self::REVIEWS_BATCH_SIZE) === 0) {
                $manager->flush();
                $manager->clear();

                $hotels = $manager->getRepository("App:Hotel")
                    ->findAll();
            }
        }

        $manager->flush();
        $manager->clear();
    }
}
