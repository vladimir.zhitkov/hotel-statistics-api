<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class RequestHandlerException extends \Exception
{
    public static function NotFoundException(string $message): RequestHandlerException
    {
        return new static($message, Response::HTTP_NOT_FOUND);
    }

    public static function BadRequestException(string $message): RequestHandlerException
    {
        return new static($message, Response::HTTP_BAD_REQUEST);
    }
}