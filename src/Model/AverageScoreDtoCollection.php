<?php

namespace App\Model;

use \Iterator;

class AverageScoreDtoCollection implements Iterator
{
    /**
     * @var AverageScoreDto[]
     */
    private $averageScores = [];

    /**
     * @param AverageScoreDto[] $averageScores
     */
    public function __construct(array $averageScores = [])
    {
        $this->averageScores = $averageScores;
    }

    public function rewind()
    {
        return reset($this->averageScores);
    }

    public function current()
    {
        return current($this->averageScores);
    }

    public function key()
    {
        return key($this->averageScores);
    }

    public function next()
    {
        return next($this->averageScores);
    }

    public function valid()
    {
        return key($this->averageScores) !== null;
    }

    public function add(AverageScoreDto $averageScore): self
    {
        $this->averageScores[] = $averageScore;
        return $this;
    }

    public function remove(string $key): self
    {
        unset($this->averageScores[$key]);
        return $this;
    }

    public function count(): int
    {
        return count($this->averageScores);
    }

    /**
     * @return AverageScoreDto[]
     */
    public function getAverageScores(): array
    {
        return $this->averageScores;
    }
}