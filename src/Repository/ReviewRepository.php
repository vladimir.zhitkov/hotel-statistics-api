<?php

namespace App\Repository;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Service\AverageScoreService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use \DateTime;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * @param Hotel $hotel
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param string $dateGroup
     * @return array
     */
    public function getByHotelForDateRange(Hotel $hotel, DateTime $startDate, DateTime $endDate, string $dateGroup = AverageScoreService::DATE_GROUP_DAILY): array
    {
        switch ($dateGroup) {
            case AverageScoreService::DATE_GROUP_WEEKLY:
                $selectPart = 'r, WEEK(r.createdDate, 1) AS dateRange';
                $orderPart = 'WEEK(r.createdDate, 1)';
                break;
            case AverageScoreService::DATE_GROUP_MONTHLY:
                $selectPart = 'r, MONTH(r.createdDate) AS dateRange';
                $orderPart = 'MONTH(r.createdDate)';
                break;
            default:
                $selectPart = 'r, DATE_FORMAT(r.createdDate, \'%Y-%m-%d\') AS dateRange';
                $orderPart = 'r.createdDate';
        }

        return $this->createQueryBuilder('r')
            ->select($selectPart)
            ->andWhere('r.hotel = :hotel')
            ->setParameter('hotel', $hotel)
            ->andWhere('r.createdDate >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('r.createdDate <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy($orderPart)
            ->getQuery()
            ->getResult()
        ;
    }
}
