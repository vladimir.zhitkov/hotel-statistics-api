<?php

namespace App\Request;

use App\Entity\Hotel;
use \DateTime;

class AverageScoreRequest
{
    /**
     * @var Hotel
     */
    private $hotel;

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    public function __construct(Hotel $hotel, DateTime $startDate, DateTime $endDate)
    {
        $this->hotel = $hotel;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }
}
