<?php

namespace App\Service;

use App\Entity\Review;
use App\Model\AverageScoreDto;
use App\Model\AverageScoreDtoCollection;
use App\Repository\ReviewRepository;
use App\Request\AverageScoreRequest;
use \DateTime;

class AverageScoreService implements AverageScoreServiceInterface
{
    public const DATE_GROUP_DAILY   = "daily";
    public const DATE_GROUP_WEEKLY  = "weekly";
    public const DATE_GROUP_MONTHLY = "monthly";

    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @param AverageScoreRequest $averageScoreRequest
     */
    public function get(AverageScoreRequest $averageScoreRequest): AverageScoreDtoCollection
    {
        $startDate = $averageScoreRequest->getStartDate();
        $endDate = $averageScoreRequest->getEndDate();

        // Include start and end days
        $endDateForInterval = clone $endDate;
        $endDateForInterval->modify("+1 day");
        $dateInterval = $endDateForInterval->diff($startDate, true);
        $numberOfDays = $dateInterval->format("%a");

        if ($numberOfDays < 30) {
            $dateGroup = self::DATE_GROUP_DAILY;
        } elseif (29 < $numberOfDays && $numberOfDays < 90) {
            $dateGroup = self::DATE_GROUP_WEEKLY;
        } else {
            $dateGroup = self::DATE_GROUP_MONTHLY;
        }

        $reviews = $this->reviewRepository->getByHotelForDateRange(
            $averageScoreRequest->getHotel(),
            $startDate,
            $endDate,
            $dateGroup
        );

        $averageScores = new AverageScoreDtoCollection();

        if (count($reviews) == 0) {
            return $averageScores;
        }

        $firstAverageScore = reset($reviews);
        $currentDateRange = $firstAverageScore['dateRange'];
        $reviewCount = 0;
        $totalScore = 0;

        // Iterate through the ordered reviews array only once
        foreach ($reviews as $review) {
            if ($currentDateRange === $review['dateRange']) {
                $reviewCount++;
                /**
                 * @var Review $reviewObject
                 */
                $reviewObject = $review[0];
                $totalScore += $reviewObject->getScore();
                continue;
            }

            $averageScores = $this->addNewAverageScore($averageScores, $reviewCount, $totalScore, $currentDateRange);

            $reviewCount = 0;
            $totalScore = 0;
            $currentDateRange = $review['dateRange'];
        }

        if ($reviewCount > 0) {
            $averageScores = $this->addNewAverageScore($averageScores, $reviewCount, $totalScore, $currentDateRange);
        }

        return $averageScores;
    }

    /**
     * @param AverageScoreDtoCollection $averageScores
     * @param int $reviewCount
     * @param int $totalScore
     * @param $dateRange
     * @return AverageScoreDtoCollection
     */
    private function addNewAverageScore(AverageScoreDtoCollection $averageScores, int $reviewCount, int $totalScore, $dateRange): AverageScoreDtoCollection
    {
        $averageScoreValue = ($reviewCount != 0) ? $totalScore / $reviewCount : 0;
        if ($dateRange instanceof DateTime) {
            $dateRange = $dateRange->format("Y-m-d");
        }
        $averageScore = new AverageScoreDto($reviewCount, $averageScoreValue, $dateRange);
        $averageScores->add($averageScore);

        return $averageScores;
    }
}
