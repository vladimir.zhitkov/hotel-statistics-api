<?php

namespace App\Service;

use App\Model\AverageScoreDtoCollection;
use App\Request\AverageScoreRequest;

interface AverageScoreServiceInterface
{
    public function get(AverageScoreRequest $averageScoreRequest): AverageScoreDtoCollection;
}
