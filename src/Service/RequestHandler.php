<?php

namespace App\Service;

use App\Entity\Hotel;
use App\Exception\RequestHandlerException;
use App\Request\AverageScoreRequest;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\HotelRepository;
use \DateTime;

class RequestHandler implements RequestHandlerInterface
{
    /**
     * @var HotelRepository
     */
    private $hotelRepository;

    public function __construct(HotelRepository $hotelRepository)
    {
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * @param Request $request
     * @param int $id
     * @return AverageScoreRequest
     * @throws RequestHandlerException
     */
    public function handleAverageScoreRequest(Request $request, int $id): AverageScoreRequest
    {
        $hotel = $this->hotelRepository->getById($id);
        if (!($hotel instanceof Hotel)) {
            throw RequestHandlerException::NotFoundException("Hotel ID not found");
        }

        $hasStartDate = $request->query->has("start-date");
        $hasEndDate = $request->query->has("end-date");
        if (!$hasStartDate || !$hasEndDate) {
            throw RequestHandlerException::BadRequestException("Missing dates");
        }

        $startDateStr = $request->query->get("start-date");
        $endDateStr = $request->query->get("end-date");

        $startDate = DateTime::createFromFormat("Y-m-d", $startDateStr);
        $endDate = DateTime::createFromFormat("Y-m-d", $endDateStr);

        if ($startDate === false || $endDate === false) {
            throw RequestHandlerException::BadRequestException("Incorrect date format. Correct format: Y-m-d");
        }

        $startDate->setTime(0, 0, 0);
        $endDate->setTime(23, 59, 59);

        if ($startDate > $endDate) {
            throw RequestHandlerException::BadRequestException("Incorrect date interval.");
        }

        return new AverageScoreRequest($hotel, $startDate, $endDate);
    }
}