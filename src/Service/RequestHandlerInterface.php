<?php

namespace App\Service;

use App\Request\AverageScoreRequest;
use Symfony\Component\HttpFoundation\Request;

interface RequestHandlerInterface
{
    public function handleAverageScoreRequest(Request $request, int $id): AverageScoreRequest;
}
