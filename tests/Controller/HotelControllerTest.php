<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\HotelController;

class HotelControllerTest extends WebTestCase
{
    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreStatusOk()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score?start-date=2020-01-01&end-date=2020-01-02'
        );
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreDailyDateGroup()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score?start-date=2020-01-01&end-date=2020-01-09'
        );
        $response = $client->getResponse();
        $responseContent = json_decode($response->getContent(), true);
        $averageScore = reset($responseContent);

        $this->assertTrue(count($responseContent) > 0);
        $this->assertNotEmpty($averageScore["reviewCount"], "reviewCount is empty");
        $this->assertNotEmpty($averageScore["averageScore"], "averageScore is empty");
        $this->assertNotEmpty($averageScore["dateGroup"], "dateGroup is empty");
    }

    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreWeeklyDateGroup()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score?start-date=2020-01-01&end-date=2020-02-25'
        );
        $response = $client->getResponse();
        $responseContent = json_decode($response->getContent(), true);
        $averageScore = reset($responseContent);

        $this->assertTrue(count($responseContent) > 0);
        $this->assertNotEmpty($averageScore["reviewCount"], "reviewCount is empty");
        $this->assertNotEmpty($averageScore["averageScore"], "averageScore is empty");
        $this->assertNotEmpty($averageScore["dateGroup"], "dateGroup is empty");
    }

    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreMonthlyDateGroup()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score?start-date=2020-01-01&end-date=2020-08-15'
        );
        $response = $client->getResponse();
        $responseContent = json_decode($response->getContent(), true);
        $averageScore = reset($responseContent);

        $this->assertTrue(count($responseContent) > 0);
        $this->assertNotEmpty($averageScore["reviewCount"], "reviewCount is empty");
        $this->assertNotEmpty($averageScore["averageScore"], "averageScore is empty");
        $this->assertNotEmpty($averageScore["dateGroup"], "dateGroup is empty");
    }

    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreBadRequest()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score'
        );
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    /**
     * @covers HotelController::getAverageScore
     */
    public function testGetAverageScoreBadRequestMissingDates()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            'hotel/1/average-score'
        );
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals("Missing dates", $responseContent["message"]);
    }
}
